# ac-forecast-app

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Run your unit tests

```
npm run test:unit
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

## Built with

- [Vue.js](https://vuejs.org/)
- [Bulma](https://bulma.io/)
- [Bulmaswatch](https://jenil.github.io/bulmaswatch/)
- [bulma-toast](https://github.com/rfoel/bulma-toast)
- [pluralize](https://github.com/blakeembrey/pluralize)
- [vue-google-charts](https://github.com/devstark-com/vue-google-charts)
- [vue-tagsinput](https://github.com/voerro/vue-tagsinput)

- [Favicon](https://favicon.io/favicon-generator/?t=%3E%3E%3E&ff=Akronim&fs=64&fc=%23FFFFFF&b=rounded&bc=%232f4356)
