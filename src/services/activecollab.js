const AC_API_URL = process.env.VUE_APP_AC_API_URL
const AC_TOKEN = process.env.VUE_APP_AC_TOKEN

const AC_HEADERS = {
  // 'Content-Type': 'application/json; charset=utf-8',
  'X-Angie-AuthApiToken': AC_TOKEN
}

async function doGet(url = 'info', params = {}) {
  url = new URL(`${AC_API_URL}/${url}`)
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))

  return await fetch(url, { headers: AC_HEADERS })
    .then(status)
    .then(json)
}

function status(response) {
  if (response.ok) {
    return Promise.resolve(response)
  } else {
    return Promise.reject(new Error(response.statusText))
  }
}

function json(response) {
  return response.json()
}

async function getLabels() {
  const labels = new Map()

  const data = await doGet('projects/labels')
  data.forEach(l => labels.set(l.id, l.name))

  return labels
}

async function getProjects() {
  let data = []
  let page = 0
  let response = null

  do {
    response = await doGet('projects', { page: page++ })
    data = data.concat(response)
  } while (response && response.length > 0)

  let projects = new Map()
  data.forEach(p => projects.set(p.id, p))

  return projects
}

async function getTasks() {
  const data = await doGet('reports/run', {
    type: 'AssignmentFilter',
    completed_on_filter: 'is_not_set',
    project_filter: 'active',
    include_tracking_data: 1
  })

  return Object.values(data.all.assignments)
}

async function getTeams() {
  return await doGet('teams')
}

async function getUsers() {
  let users = new Map()
  users.set(0, '0 Not assigned')

  const data = await doGet('users/all')
  data
    // .filter(item => team.includes(item.id))
    .forEach(p => users.set(p.id, p.display_name))

  return users
}

export { getLabels, getProjects, getTasks, getTeams, getUsers }
