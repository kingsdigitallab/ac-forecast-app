import { toast } from 'bulma-toast'

function handleError(error) {
  showNotification(error, 'is-danger')
}

function showNotification(message, type) {
  toast({
    closeOnClick: true,
    dismissible: true,
    duration: 5000,
    message: String(message),
    pauseOnHover: true,
    position: 'top-center',
    type: type
  })
}

export { handleError, showNotification }
