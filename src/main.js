import VoerroTagsInput from '@voerro/vue-tagsinput'
import pluralize from 'pluralize'
import Vue from 'vue'
import { GChart } from 'vue-google-charts'
import App from './App.vue'

require('@/assets/scss/main.scss')

Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app')

Vue.component('gchart', GChart)
Vue.component('tags-input', VoerroTagsInput)

Vue.filter('pluralize', function(value, number) {
  return pluralize(value, number)
})
