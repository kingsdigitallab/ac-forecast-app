import { shallowMount } from '@vue/test-utils'
import AcForecast from '@/components/AcForecast.vue'

describe('AcForecast.vue', () => {
  it('renders timeline chart and filters', () => {
    const expected = 'ActiveCollab Forecast'
    const wrapper = shallowMount(AcForecast)
    expect(wrapper.text()).toMatch(expected)
  })
})
